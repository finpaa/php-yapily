<?php

namespace Finpaa\TTP;

use Finpaa\Finpaa;

class Yapily
{
    private static $institutions;
    private static $preAIS;
    private static $prePIS;
    private static $singleAIS;
    private static $singlePIS;
    private static $embeddedAIS;
    private static $embeddedPIS;

    const SINGLE_AUTH = 'SINGLE_AUTH'; 
    const PRE_AUTH = 'PRE_AUTH'; 
    const EMBEDDED_AUTH = 'EMBEDDED_AUTH'; 

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        $sequences = $finpaa->Afghanistan()->{env('FINPAA_ENVIRONMENT')}()->Yapily();
        
        self::$institutions = $sequences->Institutions();

        self::$preAIS = $sequences->Pre_AIS();
        self::$prePIS = $sequences->Pre_PIS();
        self::$singleAIS = $sequences->Single_AIS();
        self::$singlePIS = $sequences->Single_PIS();
        self::$embeddedAIS = $sequences->Embedded_AIS();
        self::$embeddedPIS = $sequences->Embedded_PIS();
    }

    private static function getSequenceCode($name) {
      if(self::$$name) {
        return self::$$name;
      }
      else {
        self::selfConstruct();
        return self::getSequenceCode($name);
      }
    }

    private static function executeSequenceMethod($code, $methodIndex, $alterations, $name, $returnPayload)
    {
        $sequence = Finpaa::getSequenceMethods($code);

        if (isset($sequence->SequenceExecutions)) {

          if($methodIndex < count($sequence->SequenceExecutions))
          {
            $response = Finpaa::executeTheMethod(
              $sequence->SequenceExecutions[$methodIndex]->methodCode, $alterations, $returnPayload
            );

            return array('error' => false, 'response' => json_decode(json_encode($response), true));
          }
          else {
              return array('error' => true, 'message' => $name . ' method Failed -> Index out of bound',
                'methodIndex' => $methodIndex, 'methodsCount' => count($sequence->SequenceExecutions)
              );
          }
        }
        else {
          return array('error' => true, 
            'message' => $name .' -> No sequence executions', 'response' => json_decode(json_encode($sequence), true)
          );
       }
    }

    public static function getAuthFlow($features)
    {
        /*
         1. Single redirect flow 
          Will contain the INITIATE_DOMESTIC_SINGLE_PAYMENT feature
          Will not contain both the INITIATE_PRE_AUTHORISATION and INITIATE_EMBEDDED_DOMESTIC_SINGLE_PAYMENT features

        2. Pre-authorization flow
          Will contain the INITIATE_DOMESTIC_SINGLE_PAYMENT and INITIATE_PRE_AUTHORISATION features

        3. Embedded flow
          Will have the INITIATE_EMBEDDED_DOMESTIC_SINGLE_PAYMENT feature
        */

        if (
          in_array("INITIATE_DOMESTIC_SINGLE_PAYMENT", $features) &&
          !in_array("INITIATE_PRE_AUTHORISATION", $features) &&
          !in_array("INITIATE_EMBEDDED_DOMESTIC_SINGLE_PAYMENT", $features)
        ) {
            return self::SINGLE_AUTH;
        }
        else if (
          in_array("INITIATE_DOMESTIC_SINGLE_PAYMENT", $features) &&
          in_array("INITIATE_PRE_AUTHORISATION", $features) 
        ) {
            return self::PRE_AUTH;
        }
        else if (in_array("INITIATE_EMBEDDED_DOMESTIC_SINGLE_PAYMENT", $features)) {
            return self::EMBEDDED_AUTH;
        }

        return null;
    }

    public static function getInstitutions($alterations = [])
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('institutions'), 0, $alterations, 'Institutions', false
        );
    }
    public static function singleAis($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('singleAIS'), $methodIndex, $alterations, 'Yapily AIS Signle Sign', $returnPayload
        );
    }
    public static function singlePis($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('singlePIS'), $methodIndex, $alterations, 'Yapily PIS Signle Sign', $returnPayload
        );
    }

    public static function preAis($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('preAIS'), $methodIndex, $alterations, 'Yapily AIS Pre Auth', $returnPayload
        );
    }
    public static function prePis($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('prePIS'), $methodIndex, $alterations, 'Yapily PIS Pre Auth', $returnPayload
        );
    }

    public static function embeddedAis($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('embeddedAIS'), $methodIndex, $alterations, 'Yapily AIS embedded Auth', $returnPayload
        );
    }
    public static function embeddedPis($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('embeddedPIS'), $methodIndex, $alterations, 'Yapily PIS embedded Auth', $returnPayload
        );
    }
}

